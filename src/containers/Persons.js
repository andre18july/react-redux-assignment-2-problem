import React, { Component } from 'react';
import { connect } from 'react-redux';

import Person from '../components/Person/Person';
import AddPerson from '../components/AddPerson/AddPerson';

class Persons extends Component {

    render () {
        return (
            <div>
                <AddPerson personAdded={this.props.personAddedHandler} />
                {this.props.persons.map(person => (
                    <Person 
                        key={person.id}
                        name={person.name} 
                        age={person.age} 
                        clicked={() => this.props.personDeletedHandler(person.id)}/>
                ))}
            </div>
        );
    }
}

const mapStateToProps = state => {
    return{
        persons: state.persons
    }
}

const mapStateToDispatch = dispatch => {
    return{
        personAddedHandler: (personName,personAge) => { 
            //console.log('mapStateToDispatch -- personAddedHandler'); 
            return dispatch({type: 'ADDPERSON', personData: { personName: personName, personAge: personAge} }) 
        },
        personDeletedHandler: (personId) => { 
            //console.log(personId); 
            return dispatch({type: 'DELPERSON', personId: personId}); 
        }
    }
}

export default connect(mapStateToProps, mapStateToDispatch)(Persons);