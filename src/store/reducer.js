const initialState = {
    persons: []
}

const reducer = (state = initialState, action) => {

    switch(action.type){
        case 'ADDPERSON':
            //console.log('Reducer -- ADDPERSON');
            const newPerson = {
                id: Math.random(), // not really unique but good enough here!
                name: action.personData.personName,
                age: action.personData.personAge
            }
            return { 
                ...state,
                persons: state.persons.concat(newPerson) 
            }
        case 'DELPERSON':
            return { 
                ...state,
                persons: state.persons.filter(person => person.id !== action.personId)
            }
        default:
            break;

            
    }

    return state;
}

export default reducer;